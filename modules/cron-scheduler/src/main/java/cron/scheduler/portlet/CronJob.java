package cron.scheduler.portlet;

public interface CronJob<T> {
	/**
	 * The service property that specifies the cron schedule. The type is
	 * String+.
	 */
	String	CRON	= "cron";

	/**
	 * Run a cron job.
	 * 
	 * @param data
	 *            The data for the job
	 * @throws Exception
	 */
	public void run(T data) throws Exception;
}