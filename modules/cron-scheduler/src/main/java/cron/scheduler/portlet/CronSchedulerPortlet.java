package cron.scheduler.portlet;

import org.osgi.service.component.annotations.Component;



/**
 * @author tana0616
 */

@Component(
		immediate = true,
	    property = {CronJob.CRON + "=0/5 * * * * ?"},	    
		service = CronJob.class
	  )
public class CronSchedulerPortlet implements CronJob {

	@Override
	public void run(Object data) throws Exception {
		System.out.println("CronSchedulerPortlet");
		
	}
}