
<%@ include file="init.jsp" %>
<p>
	<b><liferay-ui:message key="test-training-session.caption"/></b>
</p>
<portlet:actionURL name="training" var="checkUserURL" />
 <portlet:resourceURL var="testAjaxResourceUrl" id="/test/osgi/resourceUrl"/>
 <portlet:renderURL var="editEntryURL">
    <portlet:param name="mvcRenderCommandName" value="/test/TrainingRenderCommand" />   
</portlet:renderURL>

 <portlet:renderURL var="showDetails">
    <portlet:param name="mvcRenderCommandName" value="/test/showdetails" />   
</portlet:renderURL>

<div class="container">  
<a href="${showDetails }">Show Details</a>
  <form class="form-horizontal" action="<%=checkUserURL%>" method="post" >
    <div class="form-group">
      <label class="control-label col-sm-2" for="employee">Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="employee" placeholder="Employee Name" name="employee">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="address">Address</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="address" placeholder="Address" name="address">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="department">Department</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="department" placeholder="Department" name="department">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Date of Birth:</label>
      <div class="col-sm-10">          
        <input type="date" class="form-control" id="dob" placeholder="Enter DoB" name="dob">
      </div>
    </div>   
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form> 
 
  
  
<button onclick="ajaxCall()">resourceURL in Ajax</button>
<a href="${editEntryURL }">click me</a>
</div>
<script type="text/javascript">
    function ajaxCall() {
        AUI().use('aui-io-request', function (A) {
            A.io.request('${testAjaxResourceUrl}', {
                method: 'post',
                data: {
                    <portlet:namespace/>sampleParam: 'Hello from jsp',
                },
                on: {
                    success: function () {
                        alert(this.get('responseData'));
                    }

                }
            });
        });
    }
</script>