<%@ include file="init.jsp" %>
<portlet:actionURL name="editProp" var="editURL"/>

<form action="<%=editURL%>" method="post" >
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="emailid" aria-describedby="emailHelp" placeholder="Enter email" value="${emailId}">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="Address">Address</label>
    <input type="text" class="form-control" id="Address" placeholder="Address" name="address" value="${address}">
  </div> 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>