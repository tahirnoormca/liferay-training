<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.tahir.trainingsession.configuration.ExampleConfiguration"%>
<p>
    <b>Hello from the Example Configuration portlet!</b>
</p>

<%
String favoriteColor=null;
try{
	


ExampleConfiguration configuration = (ExampleConfiguration) GetterUtil.getObject(request.getAttribute(ExampleConfiguration.class.getName()));
System.out.println("configuration : "+configuration);
favoriteColor = configuration.favoriteColor();

}catch(Exception e){
	System.out.println(e);
}
%>

<p>Favorite color: <span style="color: <%= favoriteColor %>;"><%= favoriteColor %></span></p>