<%@ include file="init.jsp" %>
 <div>
  <%
//orderByCol is the column name passed in the request while sorting
String orderByCol = ParamUtil.getString(request, "orderByCol"); 

//orderByType is passed in the request while sorting. It can be either asc or desc
String orderByType = ParamUtil.getString(request, "orderByType");
String sortingOrder = orderByType;
//Logic for toggle asc and desc
if(orderByType.equals("desc")){
orderByType = "asc";
}else{   
orderByType = "desc";
}

if(Validator.isNull(orderByType)){
orderByType = "desc";
}


List<Employee> employeeList=(List<Employee>) request.getAttribute("employeeList");
%>
<liferay-ui:search-container total="<%=employeeList.size()%>" var="searchContainer" delta="5" deltaConfigurable="true" 
  emptyResultsMessage="No Data available"  rowChecker="<%=new RowChecker(renderResponse)%>">
  
 <liferay-ui:search-container-results  > 
 <%
					List<Employee> employeePerPage= ListUtil.subList(employeeList, searchContainer.getStart(),searchContainer.getEnd()) ;
				  //From usersPerPage a new list sortableUsers is created. For sorting we will use this list
				    List<Employee> sortableEmployeeListPerPage = new ArrayList<Employee>(employeePerPage);
				    if(Validator.isNotNull(orderByCol)){
				        //Pass the column name to BeanComparator to get comparator object
				        BeanComparator comparator = new BeanComparator(orderByCol);
				        if(sortingOrder.equalsIgnoreCase("asc")){
				             
				            //It will sort in ascending order
				            Collections.sort(sortableEmployeeListPerPage, comparator);
				        }else{
				            //It will sort in descending order
				            Collections.reverse(sortableEmployeeListPerPage);
				        }
				 
				    }
				 
				    //sortableUsers list is sorted on the basis of condition. When page load it wont be sorted
				    //It will be sorted only when a header of coulmn is clicked for sorting
				    
					
					
					pageContext.setAttribute("results", sortableEmployeeListPerPage);
					 pageContext.setAttribute("total", employeeList.size());
					%>
 </liferay-ui:search-container-results>
 <liferay-ui:search-container-row className="back.end.module.model.Employee" modelVar="employee" keyProperty="eid" >
	   <liferay-ui:search-container-column-text name="Employee Id" value="${employee.eid}"/>
	   <liferay-ui:search-container-column-text name="Name" property="name" orderable="true" orderableProperty="name"/>
	   <liferay-ui:search-container-column-text name="Address" property="address" orderable="true" orderableProperty="address"/>   
  </liferay-ui:search-container-row>
 <liferay-ui:search-iterator />

</liferay-ui:search-container>  
  
  </div>