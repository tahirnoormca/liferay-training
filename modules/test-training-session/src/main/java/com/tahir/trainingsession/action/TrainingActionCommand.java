/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tahir.trainingsession.action;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;

import back.end.module.model.Department;
import back.end.module.model.Employee;
import back.end.module.service.DepartmentLocalService;
import back.end.module.service.EmployeeLocalService;
import back.end.module.service.EmployeeLocalServiceUtil;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	immediate = true,
	property = {
        "javax.portlet.name=liferayTrainingSession",
        "mvc.command.name=training"
    },
    service = MVCActionCommand.class
)
public class TrainingActionCommand extends BaseMVCActionCommand  {



	//private FooLocalService fooLocalService;

	@Reference(bind="-")
	private DepartmentLocalService departmentLocalService;

	@Reference(bind="-")
	private EmployeeLocalService employeeLocalService;

	private void _handleActionCommand(ActionRequest actionRequest) {


		/*HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(actionRequest);
		HttpServletRequest httpOrigReq = PortalUtil.getOriginalServletRequest(httpReq);
		String department = httpOrigReq.getParameter("department");
		System.out.println("Department: "+department);*/



		String name = ParamUtil.get(actionRequest, "department", StringPool.BLANK);
		_log.info("name "+name);
		//fooLocalService.addInToFoo(name);

		Employee emp=employeeLocalService.addEmployee(ParamUtil.getString(actionRequest, "employee"),ParamUtil.getString(actionRequest, "address"));


		Department dept1=departmentLocalService.addDepartment(ParamUtil.getString(actionRequest, "department"));
		Department dept2=departmentLocalService.addDepartment("NCRSS");
		departmentLocalService.addEmployeeDepartments(emp.getEid(), new long[]{dept1.getDeptid(),dept2.getDeptid()});


	}

	private static final Log _log = LogFactoryUtil.getLog(
		TrainingActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		_handleActionCommand(actionRequest);
	}
}