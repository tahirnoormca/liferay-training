package com.tahir.trainingsession.portlet;

import java.io.IOException;
import java.util.Map;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import org.osgi.service.component.annotations.Component;

import org.osgi.service.component.annotations.Reference;


import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.tahir.trainingsession.configuration.ExampleConfiguration;
import org.osgi.service.component.annotations.Activate;

import org.osgi.service.component.annotations.Modified;

import back.end.module.service.EmployeeLocalService;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import aQute.bnd.annotation.metatype.Configurable;


/**
 * @author tana0616
 */
@Component(
	configurationPid = "com.tahir.trainingsession.configuration.ExampleConfiguration",
   	immediate = true,
	property = {
		
		"javax.portlet.name=liferayTrainingSession",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=test-training-session Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.portlet-mode=text/html;view,edit",
		"javax.portlet.init-param.view-template=/jsps/view.jsp",
		"javax.portlet.init-param.edit-template=/jsps/edit.jsp",
		"javax.portlet.init-param.config-template=/jsps/config.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		
	},
	service = Portlet.class
)
public class TestTrainingSession2Portlet extends MVCPortlet {
	
	@Reference(bind="-")
	private EmployeeLocalService employeeLocalService;
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {	
		System.out.println("_configuration : "+_configuration);
		renderRequest.setAttribute(ExampleConfiguration.class.getName(), _configuration);
		renderRequest.setAttribute("employeeList",employeeLocalService.getEmployees(0, employeeLocalService.getEmployeesCount()));		
		super.doView(renderRequest, renderResponse);
	}
	@Override
	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		PortletPreferences portletPreferences = renderRequest.getPreferences();	
		renderRequest.setAttribute("emailId",portletPreferences.getValue("emailId", ""));
		renderRequest.setAttribute("address",portletPreferences.getValue("address", ""));
		System.out.println("Edit mode");
		super.doEdit(renderRequest, renderResponse);
	}
	@ProcessAction(name = "editProp")
	public void editProp(ActionRequest actionRequest, ActionResponse actionResponse) throws ValidatorException, IOException, ReadOnlyException {
		    PortletPreferences portletPreferences = actionRequest.getPreferences();		
			portletPreferences.setValue("emailId",ParamUtil.getString(actionRequest,"emailid"));
			portletPreferences.setValue("address",ParamUtil.getString(actionRequest,"address"));
			portletPreferences.store();		
			System.out.println("Editpage");
	}
	
	
	public String getFavoriteColor(Map colors) {
		System.out.println("getFavoriteColor");
		return (String) colors.get(_configuration.favoriteColor());
	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		System.out.println("activate");
		_configuration = Configurable.createConfigurable(
		ExampleConfiguration.class, properties);
	}	

	private volatile ExampleConfiguration _configuration;
	
}