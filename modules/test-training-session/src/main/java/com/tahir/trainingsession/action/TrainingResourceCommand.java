package com.tahir.trainingsession.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import org.osgi.service.component.annotations.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;

@Component(
		immediate = true,
		property = {
	        "javax.portlet.name=liferayTrainingSession",
	        "mvc.command.name=/test/osgi/resourceUrl"
	    },
	    service = MVCResourceCommand.class
	)
public class TrainingResourceCommand extends BaseMVCResourceCommand{

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception ,IOException {
		System.out.println("Ajax");
		resourceResponse.setContentType("text/html");
		ArrayList<String> ls=new ArrayList<>();
		ls.add("tahir");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
	    String json = gson.toJson(ls);
		PrintWriter out = resourceResponse.getWriter();
		out.println(json);
		out.flush();



	}

}
