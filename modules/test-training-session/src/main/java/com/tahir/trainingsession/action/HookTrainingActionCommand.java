package com.tahir.trainingsession.action;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;

@Component(
		immediate = true,
		property = {
	        "javax.portlet.name=liferayTrainingSession",
	        "mvc.command.name=training",
	        "service.ranking:Integer=1"
	    },
	    service = MVCActionCommand.class
	)
public class HookTrainingActionCommand extends BaseMVCActionCommand{
	
	
	 @Reference(target = "(&(mvc.command.name=training)" +
             "(javax.portlet.name=liferayTrainingSession)" +
             "(component.name=com.tahir.trainingsession.action.TrainingActionCommand))")
protected MVCActionCommand mvcActionCommand;
	
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		System.out.println("hooooooooooooooook");
		 mvcActionCommand.processAction(actionRequest,actionResponse);
	}

}
