package com.tahir.trainingsession.action;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

@Component(
		immediate = true,
		property = {
	        "javax.portlet.name=liferayTrainingSession",
	        "mvc.command.name=/test/TrainingRenderCommand"
	    },
	    service = MVCRenderCommand.class
	)
public class TrainingRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		System.out.println(">>>>>>>>>>>>> : TrainingRenderCommand");
		return "/jsps/render.jsp";
	}

}
