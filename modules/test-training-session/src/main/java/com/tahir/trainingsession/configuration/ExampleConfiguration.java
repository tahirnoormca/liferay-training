package com.tahir.trainingsession.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.tahir.trainingsession.configuration.ExampleConfiguration")
public interface ExampleConfiguration {
	
	 @Meta.AD(
		    	deflt = "blue",
		    	required = false
		    )
		    public String favoriteColor();

		    @Meta.AD(
		       deflt = "red|green|blue|yellow",
		       required = false
		    )
		    public String[] validColors();

		    @Meta.AD(required = false)
		    public int favoriteNumber();

}
