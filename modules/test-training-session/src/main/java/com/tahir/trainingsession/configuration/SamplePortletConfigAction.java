package com.tahir.trainingsession.configuration;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

@Component(
		configurationPid = "com.tahir.trainingsession.configuration.ExampleConfiguration",
        configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true,
        property = {
        		"javax.portlet.name=liferayTrainingSession",
        },
        service = ConfigurationAction.class
    )
public class SamplePortletConfigAction extends DefaultConfigurationAction {
	@Override
	protected void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		System.out.println("SamplePortletConfigAction");
		super.doView(request, response);
	}

}
