create table Department (
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	deptid LONG not null primary key,
	department VARCHAR(75) null
);

create table Employee (
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	eid LONG not null primary key,
	name VARCHAR(75) null,
	address VARCHAR(75) null
);

create table FOO_Employee_department (
	companyId LONG not null,
	deptid LONG not null,
	eid LONG not null,
	primary key (deptid, eid)
);