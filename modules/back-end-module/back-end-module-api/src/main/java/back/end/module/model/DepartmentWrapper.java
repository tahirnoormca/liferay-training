/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package back.end.module.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Department}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Department
 * @generated
 */
@ProviderType
public class DepartmentWrapper implements Department, ModelWrapper<Department> {
	public DepartmentWrapper(Department department) {
		_department = department;
	}

	@Override
	public Class<?> getModelClass() {
		return Department.class;
	}

	@Override
	public String getModelClassName() {
		return Department.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("deptid", getDeptid());
		attributes.put("department", getDepartment());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long deptid = (Long)attributes.get("deptid");

		if (deptid != null) {
			setDeptid(deptid);
		}

		String department = (String)attributes.get("department");

		if (department != null) {
			setDepartment(department);
		}
	}

	@Override
	public back.end.module.model.Department toEscapedModel() {
		return new DepartmentWrapper(_department.toEscapedModel());
	}

	@Override
	public back.end.module.model.Department toUnescapedModel() {
		return new DepartmentWrapper(_department.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _department.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _department.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _department.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _department.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<back.end.module.model.Department> toCacheModel() {
		return _department.toCacheModel();
	}

	@Override
	public int compareTo(back.end.module.model.Department department) {
		return _department.compareTo(department);
	}

	@Override
	public int hashCode() {
		return _department.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _department.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new DepartmentWrapper((Department)_department.clone());
	}

	/**
	* Returns the department of this department.
	*
	* @return the department of this department
	*/
	@Override
	public java.lang.String getDepartment() {
		return _department.getDepartment();
	}

	/**
	* Returns the user name of this department.
	*
	* @return the user name of this department
	*/
	@Override
	public java.lang.String getUserName() {
		return _department.getUserName();
	}

	/**
	* Returns the user uuid of this department.
	*
	* @return the user uuid of this department
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _department.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _department.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _department.toXmlString();
	}

	/**
	* Returns the create date of this department.
	*
	* @return the create date of this department
	*/
	@Override
	public Date getCreateDate() {
		return _department.getCreateDate();
	}

	/**
	* Returns the modified date of this department.
	*
	* @return the modified date of this department
	*/
	@Override
	public Date getModifiedDate() {
		return _department.getModifiedDate();
	}

	/**
	* Returns the company ID of this department.
	*
	* @return the company ID of this department
	*/
	@Override
	public long getCompanyId() {
		return _department.getCompanyId();
	}

	/**
	* Returns the deptid of this department.
	*
	* @return the deptid of this department
	*/
	@Override
	public long getDeptid() {
		return _department.getDeptid();
	}

	/**
	* Returns the group ID of this department.
	*
	* @return the group ID of this department
	*/
	@Override
	public long getGroupId() {
		return _department.getGroupId();
	}

	/**
	* Returns the primary key of this department.
	*
	* @return the primary key of this department
	*/
	@Override
	public long getPrimaryKey() {
		return _department.getPrimaryKey();
	}

	/**
	* Returns the user ID of this department.
	*
	* @return the user ID of this department
	*/
	@Override
	public long getUserId() {
		return _department.getUserId();
	}

	@Override
	public void persist() {
		_department.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_department.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this department.
	*
	* @param companyId the company ID of this department
	*/
	@Override
	public void setCompanyId(long companyId) {
		_department.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this department.
	*
	* @param createDate the create date of this department
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_department.setCreateDate(createDate);
	}

	/**
	* Sets the department of this department.
	*
	* @param department the department of this department
	*/
	@Override
	public void setDepartment(java.lang.String department) {
		_department.setDepartment(department);
	}

	/**
	* Sets the deptid of this department.
	*
	* @param deptid the deptid of this department
	*/
	@Override
	public void setDeptid(long deptid) {
		_department.setDeptid(deptid);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_department.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_department.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_department.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this department.
	*
	* @param groupId the group ID of this department
	*/
	@Override
	public void setGroupId(long groupId) {
		_department.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this department.
	*
	* @param modifiedDate the modified date of this department
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_department.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_department.setNew(n);
	}

	/**
	* Sets the primary key of this department.
	*
	* @param primaryKey the primary key of this department
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_department.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_department.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this department.
	*
	* @param userId the user ID of this department
	*/
	@Override
	public void setUserId(long userId) {
		_department.setUserId(userId);
	}

	/**
	* Sets the user name of this department.
	*
	* @param userName the user name of this department
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_department.setUserName(userName);
	}

	/**
	* Sets the user uuid of this department.
	*
	* @param userUuid the user uuid of this department
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_department.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DepartmentWrapper)) {
			return false;
		}

		DepartmentWrapper departmentWrapper = (DepartmentWrapper)obj;

		if (Objects.equals(_department, departmentWrapper._department)) {
			return true;
		}

		return false;
	}

	@Override
	public Department getWrappedModel() {
		return _department;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _department.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _department.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_department.resetOriginalValues();
	}

	private final Department _department;
}