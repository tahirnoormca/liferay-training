/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package back.end.module.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DepartmentLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DepartmentLocalService
 * @generated
 */
@ProviderType
public class DepartmentLocalServiceWrapper implements DepartmentLocalService,
	ServiceWrapper<DepartmentLocalService> {
	public DepartmentLocalServiceWrapper(
		DepartmentLocalService departmentLocalService) {
		_departmentLocalService = departmentLocalService;
	}

	/**
	* Adds the department to the database. Also notifies the appropriate model listeners.
	*
	* @param department the department
	* @return the department that was added
	*/
	@Override
	public back.end.module.model.Department addDepartment(
		back.end.module.model.Department department) {
		return _departmentLocalService.addDepartment(department);
	}

	@Override
	public back.end.module.model.Department addDepartment(
		java.lang.String departmentname) {
		return _departmentLocalService.addDepartment(departmentname);
	}

	/**
	* Creates a new department with the primary key. Does not add the department to the database.
	*
	* @param deptid the primary key for the new department
	* @return the new department
	*/
	@Override
	public back.end.module.model.Department createDepartment(long deptid) {
		return _departmentLocalService.createDepartment(deptid);
	}

	/**
	* Deletes the department from the database. Also notifies the appropriate model listeners.
	*
	* @param department the department
	* @return the department that was removed
	*/
	@Override
	public back.end.module.model.Department deleteDepartment(
		back.end.module.model.Department department) {
		return _departmentLocalService.deleteDepartment(department);
	}

	/**
	* Deletes the department with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deptid the primary key of the department
	* @return the department that was removed
	* @throws PortalException if a department with the primary key could not be found
	*/
	@Override
	public back.end.module.model.Department deleteDepartment(long deptid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _departmentLocalService.deleteDepartment(deptid);
	}

	@Override
	public back.end.module.model.Department fetchDepartment(long deptid) {
		return _departmentLocalService.fetchDepartment(deptid);
	}

	/**
	* Returns the department with the primary key.
	*
	* @param deptid the primary key of the department
	* @return the department
	* @throws PortalException if a department with the primary key could not be found
	*/
	@Override
	public back.end.module.model.Department getDepartment(long deptid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _departmentLocalService.getDepartment(deptid);
	}

	/**
	* Updates the department in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param department the department
	* @return the department that was updated
	*/
	@Override
	public back.end.module.model.Department updateDepartment(
		back.end.module.model.Department department) {
		return _departmentLocalService.updateDepartment(department);
	}

	@Override
	public boolean hasEmployeeDepartment(long eid, long deptid) {
		return _departmentLocalService.hasEmployeeDepartment(eid, deptid);
	}

	@Override
	public boolean hasEmployeeDepartments(long eid) {
		return _departmentLocalService.hasEmployeeDepartments(eid);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _departmentLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _departmentLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _departmentLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _departmentLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _departmentLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of departments.
	*
	* @return the number of departments
	*/
	@Override
	public int getDepartmentsCount() {
		return _departmentLocalService.getDepartmentsCount();
	}

	@Override
	public int getEmployeeDepartmentsCount(long eid) {
		return _departmentLocalService.getEmployeeDepartmentsCount(eid);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _departmentLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _departmentLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _departmentLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _departmentLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the departments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @return the range of departments
	*/
	@Override
	public java.util.List<back.end.module.model.Department> getDepartments(
		int start, int end) {
		return _departmentLocalService.getDepartments(start, end);
	}

	@Override
	public java.util.List<back.end.module.model.Department> getEmployeeDepartments(
		long eid) {
		return _departmentLocalService.getEmployeeDepartments(eid);
	}

	@Override
	public java.util.List<back.end.module.model.Department> getEmployeeDepartments(
		long eid, int start, int end) {
		return _departmentLocalService.getEmployeeDepartments(eid, start, end);
	}

	@Override
	public java.util.List<back.end.module.model.Department> getEmployeeDepartments(
		long eid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<back.end.module.model.Department> orderByComparator) {
		return _departmentLocalService.getEmployeeDepartments(eid, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _departmentLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _departmentLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Returns the eids of the employees associated with the department.
	*
	* @param deptid the deptid of the department
	* @return long[] the eids of employees associated with the department
	*/
	@Override
	public long[] getEmployeePrimaryKeys(long deptid) {
		return _departmentLocalService.getEmployeePrimaryKeys(deptid);
	}

	@Override
	public void addEmployeeDepartment(long eid,
		back.end.module.model.Department department) {
		_departmentLocalService.addEmployeeDepartment(eid, department);
	}

	@Override
	public void addEmployeeDepartment(long eid, long deptid) {
		_departmentLocalService.addEmployeeDepartment(eid, deptid);
	}

	@Override
	public void addEmployeeDepartments(long eid,
		java.util.List<back.end.module.model.Department> departments) {
		_departmentLocalService.addEmployeeDepartments(eid, departments);
	}

	@Override
	public void addEmployeeDepartments(long eid, long[] deptids) {
		_departmentLocalService.addEmployeeDepartments(eid, deptids);
	}

	@Override
	public void clearEmployeeDepartments(long eid) {
		_departmentLocalService.clearEmployeeDepartments(eid);
	}

	@Override
	public void deleteEmployeeDepartment(long eid,
		back.end.module.model.Department department) {
		_departmentLocalService.deleteEmployeeDepartment(eid, department);
	}

	@Override
	public void deleteEmployeeDepartment(long eid, long deptid) {
		_departmentLocalService.deleteEmployeeDepartment(eid, deptid);
	}

	@Override
	public void deleteEmployeeDepartments(long eid,
		java.util.List<back.end.module.model.Department> departments) {
		_departmentLocalService.deleteEmployeeDepartments(eid, departments);
	}

	@Override
	public void deleteEmployeeDepartments(long eid, long[] deptids) {
		_departmentLocalService.deleteEmployeeDepartments(eid, deptids);
	}

	@Override
	public void setEmployeeDepartments(long eid, long[] deptids) {
		_departmentLocalService.setEmployeeDepartments(eid, deptids);
	}

	@Override
	public DepartmentLocalService getWrappedService() {
		return _departmentLocalService;
	}

	@Override
	public void setWrappedService(DepartmentLocalService departmentLocalService) {
		_departmentLocalService = departmentLocalService;
	}

	private DepartmentLocalService _departmentLocalService;
}