/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package back.end.module.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EmployeeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EmployeeLocalService
 * @generated
 */
@ProviderType
public class EmployeeLocalServiceWrapper implements EmployeeLocalService,
	ServiceWrapper<EmployeeLocalService> {
	public EmployeeLocalServiceWrapper(
		EmployeeLocalService employeeLocalService) {
		_employeeLocalService = employeeLocalService;
	}

	/**
	* Adds the employee to the database. Also notifies the appropriate model listeners.
	*
	* @param employee the employee
	* @return the employee that was added
	*/
	@Override
	public back.end.module.model.Employee addEmployee(
		back.end.module.model.Employee employee) {
		return _employeeLocalService.addEmployee(employee);
	}

	@Override
	public back.end.module.model.Employee addEmployee(
		java.lang.String employeeName, java.lang.String address) {
		return _employeeLocalService.addEmployee(employeeName, address);
	}

	/**
	* Creates a new employee with the primary key. Does not add the employee to the database.
	*
	* @param eid the primary key for the new employee
	* @return the new employee
	*/
	@Override
	public back.end.module.model.Employee createEmployee(long eid) {
		return _employeeLocalService.createEmployee(eid);
	}

	/**
	* Deletes the employee from the database. Also notifies the appropriate model listeners.
	*
	* @param employee the employee
	* @return the employee that was removed
	*/
	@Override
	public back.end.module.model.Employee deleteEmployee(
		back.end.module.model.Employee employee) {
		return _employeeLocalService.deleteEmployee(employee);
	}

	/**
	* Deletes the employee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param eid the primary key of the employee
	* @return the employee that was removed
	* @throws PortalException if a employee with the primary key could not be found
	*/
	@Override
	public back.end.module.model.Employee deleteEmployee(long eid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _employeeLocalService.deleteEmployee(eid);
	}

	@Override
	public back.end.module.model.Employee fetchEmployee(long eid) {
		return _employeeLocalService.fetchEmployee(eid);
	}

	/**
	* Returns the employee with the primary key.
	*
	* @param eid the primary key of the employee
	* @return the employee
	* @throws PortalException if a employee with the primary key could not be found
	*/
	@Override
	public back.end.module.model.Employee getEmployee(long eid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _employeeLocalService.getEmployee(eid);
	}

	/**
	* Updates the employee in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employee the employee
	* @return the employee that was updated
	*/
	@Override
	public back.end.module.model.Employee updateEmployee(
		back.end.module.model.Employee employee) {
		return _employeeLocalService.updateEmployee(employee);
	}

	@Override
	public boolean hasDepartmentEmployee(long deptid, long eid) {
		return _employeeLocalService.hasDepartmentEmployee(deptid, eid);
	}

	@Override
	public boolean hasDepartmentEmployees(long deptid) {
		return _employeeLocalService.hasDepartmentEmployees(deptid);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _employeeLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _employeeLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _employeeLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _employeeLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _employeeLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public int getDepartmentEmployeesCount(long deptid) {
		return _employeeLocalService.getDepartmentEmployeesCount(deptid);
	}

	/**
	* Returns the number of employees.
	*
	* @return the number of employees
	*/
	@Override
	public int getEmployeesCount() {
		return _employeeLocalService.getEmployeesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _employeeLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _employeeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.EmployeeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _employeeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.EmployeeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _employeeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<back.end.module.model.Employee> getDepartmentEmployees(
		long deptid) {
		return _employeeLocalService.getDepartmentEmployees(deptid);
	}

	@Override
	public java.util.List<back.end.module.model.Employee> getDepartmentEmployees(
		long deptid, int start, int end) {
		return _employeeLocalService.getDepartmentEmployees(deptid, start, end);
	}

	@Override
	public java.util.List<back.end.module.model.Employee> getDepartmentEmployees(
		long deptid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<back.end.module.model.Employee> orderByComparator) {
		return _employeeLocalService.getDepartmentEmployees(deptid, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the employees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.EmployeeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of employees
	* @param end the upper bound of the range of employees (not inclusive)
	* @return the range of employees
	*/
	@Override
	public java.util.List<back.end.module.model.Employee> getEmployees(
		int start, int end) {
		return _employeeLocalService.getEmployees(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _employeeLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _employeeLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the deptids of the departments associated with the employee.
	*
	* @param eid the eid of the employee
	* @return long[] the deptids of departments associated with the employee
	*/
	@Override
	public long[] getDepartmentPrimaryKeys(long eid) {
		return _employeeLocalService.getDepartmentPrimaryKeys(eid);
	}

	@Override
	public void addDepartmentEmployee(long deptid,
		back.end.module.model.Employee employee) {
		_employeeLocalService.addDepartmentEmployee(deptid, employee);
	}

	@Override
	public void addDepartmentEmployee(long deptid, long eid) {
		_employeeLocalService.addDepartmentEmployee(deptid, eid);
	}

	@Override
	public void addDepartmentEmployees(long deptid,
		java.util.List<back.end.module.model.Employee> employees) {
		_employeeLocalService.addDepartmentEmployees(deptid, employees);
	}

	@Override
	public void addDepartmentEmployees(long deptid, long[] eids) {
		_employeeLocalService.addDepartmentEmployees(deptid, eids);
	}

	@Override
	public void clearDepartmentEmployees(long deptid) {
		_employeeLocalService.clearDepartmentEmployees(deptid);
	}

	@Override
	public void deleteDepartmentEmployee(long deptid,
		back.end.module.model.Employee employee) {
		_employeeLocalService.deleteDepartmentEmployee(deptid, employee);
	}

	@Override
	public void deleteDepartmentEmployee(long deptid, long eid) {
		_employeeLocalService.deleteDepartmentEmployee(deptid, eid);
	}

	@Override
	public void deleteDepartmentEmployees(long deptid,
		java.util.List<back.end.module.model.Employee> employees) {
		_employeeLocalService.deleteDepartmentEmployees(deptid, employees);
	}

	@Override
	public void deleteDepartmentEmployees(long deptid, long[] eids) {
		_employeeLocalService.deleteDepartmentEmployees(deptid, eids);
	}

	@Override
	public void setDepartmentEmployees(long deptid, long[] eids) {
		_employeeLocalService.setDepartmentEmployees(deptid, eids);
	}

	@Override
	public EmployeeLocalService getWrappedService() {
		return _employeeLocalService;
	}

	@Override
	public void setWrappedService(EmployeeLocalService employeeLocalService) {
		_employeeLocalService = employeeLocalService;
	}

	private EmployeeLocalService _employeeLocalService;
}