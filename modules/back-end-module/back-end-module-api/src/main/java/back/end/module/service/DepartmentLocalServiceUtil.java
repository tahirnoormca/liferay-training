/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package back.end.module.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Department. This utility wraps
 * {@link back.end.module.service.impl.DepartmentLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DepartmentLocalService
 * @see back.end.module.service.base.DepartmentLocalServiceBaseImpl
 * @see back.end.module.service.impl.DepartmentLocalServiceImpl
 * @generated
 */
@ProviderType
public class DepartmentLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link back.end.module.service.impl.DepartmentLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the department to the database. Also notifies the appropriate model listeners.
	*
	* @param department the department
	* @return the department that was added
	*/
	public static back.end.module.model.Department addDepartment(
		back.end.module.model.Department department) {
		return getService().addDepartment(department);
	}

	public static back.end.module.model.Department addDepartment(
		java.lang.String departmentname) {
		return getService().addDepartment(departmentname);
	}

	/**
	* Creates a new department with the primary key. Does not add the department to the database.
	*
	* @param deptid the primary key for the new department
	* @return the new department
	*/
	public static back.end.module.model.Department createDepartment(long deptid) {
		return getService().createDepartment(deptid);
	}

	/**
	* Deletes the department from the database. Also notifies the appropriate model listeners.
	*
	* @param department the department
	* @return the department that was removed
	*/
	public static back.end.module.model.Department deleteDepartment(
		back.end.module.model.Department department) {
		return getService().deleteDepartment(department);
	}

	/**
	* Deletes the department with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deptid the primary key of the department
	* @return the department that was removed
	* @throws PortalException if a department with the primary key could not be found
	*/
	public static back.end.module.model.Department deleteDepartment(long deptid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteDepartment(deptid);
	}

	public static back.end.module.model.Department fetchDepartment(long deptid) {
		return getService().fetchDepartment(deptid);
	}

	/**
	* Returns the department with the primary key.
	*
	* @param deptid the primary key of the department
	* @return the department
	* @throws PortalException if a department with the primary key could not be found
	*/
	public static back.end.module.model.Department getDepartment(long deptid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getDepartment(deptid);
	}

	/**
	* Updates the department in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param department the department
	* @return the department that was updated
	*/
	public static back.end.module.model.Department updateDepartment(
		back.end.module.model.Department department) {
		return getService().updateDepartment(department);
	}

	public static boolean hasEmployeeDepartment(long eid, long deptid) {
		return getService().hasEmployeeDepartment(eid, deptid);
	}

	public static boolean hasEmployeeDepartments(long eid) {
		return getService().hasEmployeeDepartments(eid);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of departments.
	*
	* @return the number of departments
	*/
	public static int getDepartmentsCount() {
		return getService().getDepartmentsCount();
	}

	public static int getEmployeeDepartmentsCount(long eid) {
		return getService().getEmployeeDepartmentsCount(eid);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the departments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link back.end.module.model.impl.DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @return the range of departments
	*/
	public static java.util.List<back.end.module.model.Department> getDepartments(
		int start, int end) {
		return getService().getDepartments(start, end);
	}

	public static java.util.List<back.end.module.model.Department> getEmployeeDepartments(
		long eid) {
		return getService().getEmployeeDepartments(eid);
	}

	public static java.util.List<back.end.module.model.Department> getEmployeeDepartments(
		long eid, int start, int end) {
		return getService().getEmployeeDepartments(eid, start, end);
	}

	public static java.util.List<back.end.module.model.Department> getEmployeeDepartments(
		long eid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<back.end.module.model.Department> orderByComparator) {
		return getService()
				   .getEmployeeDepartments(eid, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the eids of the employees associated with the department.
	*
	* @param deptid the deptid of the department
	* @return long[] the eids of employees associated with the department
	*/
	public static long[] getEmployeePrimaryKeys(long deptid) {
		return getService().getEmployeePrimaryKeys(deptid);
	}

	public static void addEmployeeDepartment(long eid,
		back.end.module.model.Department department) {
		getService().addEmployeeDepartment(eid, department);
	}

	public static void addEmployeeDepartment(long eid, long deptid) {
		getService().addEmployeeDepartment(eid, deptid);
	}

	public static void addEmployeeDepartments(long eid,
		java.util.List<back.end.module.model.Department> departments) {
		getService().addEmployeeDepartments(eid, departments);
	}

	public static void addEmployeeDepartments(long eid, long[] deptids) {
		getService().addEmployeeDepartments(eid, deptids);
	}

	public static void clearEmployeeDepartments(long eid) {
		getService().clearEmployeeDepartments(eid);
	}

	public static void deleteEmployeeDepartment(long eid,
		back.end.module.model.Department department) {
		getService().deleteEmployeeDepartment(eid, department);
	}

	public static void deleteEmployeeDepartment(long eid, long deptid) {
		getService().deleteEmployeeDepartment(eid, deptid);
	}

	public static void deleteEmployeeDepartments(long eid,
		java.util.List<back.end.module.model.Department> departments) {
		getService().deleteEmployeeDepartments(eid, departments);
	}

	public static void deleteEmployeeDepartments(long eid, long[] deptids) {
		getService().deleteEmployeeDepartments(eid, deptids);
	}

	public static void setEmployeeDepartments(long eid, long[] deptids) {
		getService().setEmployeeDepartments(eid, deptids);
	}

	public static DepartmentLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DepartmentLocalService, DepartmentLocalService> _serviceTracker =
		ServiceTrackerFactory.open(DepartmentLocalService.class);
}