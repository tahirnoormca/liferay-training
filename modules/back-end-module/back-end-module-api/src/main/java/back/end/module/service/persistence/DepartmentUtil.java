/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package back.end.module.service.persistence;

import aQute.bnd.annotation.ProviderType;

import back.end.module.model.Department;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the department service. This utility wraps {@link back.end.module.service.persistence.impl.DepartmentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DepartmentPersistence
 * @see back.end.module.service.persistence.impl.DepartmentPersistenceImpl
 * @generated
 */
@ProviderType
public class DepartmentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Department department) {
		getPersistence().clearCache(department);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Department> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Department> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Department> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Department> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Department update(Department department) {
		return getPersistence().update(department);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Department update(Department department,
		ServiceContext serviceContext) {
		return getPersistence().update(department, serviceContext);
	}

	/**
	* Caches the department in the entity cache if it is enabled.
	*
	* @param department the department
	*/
	public static void cacheResult(Department department) {
		getPersistence().cacheResult(department);
	}

	/**
	* Caches the departments in the entity cache if it is enabled.
	*
	* @param departments the departments
	*/
	public static void cacheResult(List<Department> departments) {
		getPersistence().cacheResult(departments);
	}

	/**
	* Creates a new department with the primary key. Does not add the department to the database.
	*
	* @param deptid the primary key for the new department
	* @return the new department
	*/
	public static Department create(long deptid) {
		return getPersistence().create(deptid);
	}

	/**
	* Removes the department with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deptid the primary key of the department
	* @return the department that was removed
	* @throws NoSuchDepartmentException if a department with the primary key could not be found
	*/
	public static Department remove(long deptid)
		throws back.end.module.exception.NoSuchDepartmentException {
		return getPersistence().remove(deptid);
	}

	public static Department updateImpl(Department department) {
		return getPersistence().updateImpl(department);
	}

	/**
	* Returns the department with the primary key or throws a {@link NoSuchDepartmentException} if it could not be found.
	*
	* @param deptid the primary key of the department
	* @return the department
	* @throws NoSuchDepartmentException if a department with the primary key could not be found
	*/
	public static Department findByPrimaryKey(long deptid)
		throws back.end.module.exception.NoSuchDepartmentException {
		return getPersistence().findByPrimaryKey(deptid);
	}

	/**
	* Returns the department with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param deptid the primary key of the department
	* @return the department, or <code>null</code> if a department with the primary key could not be found
	*/
	public static Department fetchByPrimaryKey(long deptid) {
		return getPersistence().fetchByPrimaryKey(deptid);
	}

	public static java.util.Map<java.io.Serializable, Department> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the departments.
	*
	* @return the departments
	*/
	public static List<Department> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the departments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @return the range of departments
	*/
	public static List<Department> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the departments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of departments
	*/
	public static List<Department> findAll(int start, int end,
		OrderByComparator<Department> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the departments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of departments
	*/
	public static List<Department> findAll(int start, int end,
		OrderByComparator<Department> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the departments from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of departments.
	*
	* @return the number of departments
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	* Returns the primaryKeys of employees associated with the department.
	*
	* @param pk the primary key of the department
	* @return long[] of the primaryKeys of employees associated with the department
	*/
	public static long[] getEmployeePrimaryKeys(long pk) {
		return getPersistence().getEmployeePrimaryKeys(pk);
	}

	/**
	* Returns all the employees associated with the department.
	*
	* @param pk the primary key of the department
	* @return the employees associated with the department
	*/
	public static List<back.end.module.model.Employee> getEmployees(long pk) {
		return getPersistence().getEmployees(pk);
	}

	/**
	* Returns a range of all the employees associated with the department.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the department
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @return the range of employees associated with the department
	*/
	public static List<back.end.module.model.Employee> getEmployees(long pk,
		int start, int end) {
		return getPersistence().getEmployees(pk, start, end);
	}

	/**
	* Returns an ordered range of all the employees associated with the department.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DepartmentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the department
	* @param start the lower bound of the range of departments
	* @param end the upper bound of the range of departments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employees associated with the department
	*/
	public static List<back.end.module.model.Employee> getEmployees(long pk,
		int start, int end,
		OrderByComparator<back.end.module.model.Employee> orderByComparator) {
		return getPersistence().getEmployees(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of employees associated with the department.
	*
	* @param pk the primary key of the department
	* @return the number of employees associated with the department
	*/
	public static int getEmployeesSize(long pk) {
		return getPersistence().getEmployeesSize(pk);
	}

	/**
	* Returns <code>true</code> if the employee is associated with the department.
	*
	* @param pk the primary key of the department
	* @param employeePK the primary key of the employee
	* @return <code>true</code> if the employee is associated with the department; <code>false</code> otherwise
	*/
	public static boolean containsEmployee(long pk, long employeePK) {
		return getPersistence().containsEmployee(pk, employeePK);
	}

	/**
	* Returns <code>true</code> if the department has any employees associated with it.
	*
	* @param pk the primary key of the department to check for associations with employees
	* @return <code>true</code> if the department has any employees associated with it; <code>false</code> otherwise
	*/
	public static boolean containsEmployees(long pk) {
		return getPersistence().containsEmployees(pk);
	}

	/**
	* Adds an association between the department and the employee. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employeePK the primary key of the employee
	*/
	public static void addEmployee(long pk, long employeePK) {
		getPersistence().addEmployee(pk, employeePK);
	}

	/**
	* Adds an association between the department and the employee. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employee the employee
	*/
	public static void addEmployee(long pk,
		back.end.module.model.Employee employee) {
		getPersistence().addEmployee(pk, employee);
	}

	/**
	* Adds an association between the department and the employees. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employeePKs the primary keys of the employees
	*/
	public static void addEmployees(long pk, long[] employeePKs) {
		getPersistence().addEmployees(pk, employeePKs);
	}

	/**
	* Adds an association between the department and the employees. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employees the employees
	*/
	public static void addEmployees(long pk,
		List<back.end.module.model.Employee> employees) {
		getPersistence().addEmployees(pk, employees);
	}

	/**
	* Clears all associations between the department and its employees. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department to clear the associated employees from
	*/
	public static void clearEmployees(long pk) {
		getPersistence().clearEmployees(pk);
	}

	/**
	* Removes the association between the department and the employee. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employeePK the primary key of the employee
	*/
	public static void removeEmployee(long pk, long employeePK) {
		getPersistence().removeEmployee(pk, employeePK);
	}

	/**
	* Removes the association between the department and the employee. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employee the employee
	*/
	public static void removeEmployee(long pk,
		back.end.module.model.Employee employee) {
		getPersistence().removeEmployee(pk, employee);
	}

	/**
	* Removes the association between the department and the employees. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employeePKs the primary keys of the employees
	*/
	public static void removeEmployees(long pk, long[] employeePKs) {
		getPersistence().removeEmployees(pk, employeePKs);
	}

	/**
	* Removes the association between the department and the employees. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employees the employees
	*/
	public static void removeEmployees(long pk,
		List<back.end.module.model.Employee> employees) {
		getPersistence().removeEmployees(pk, employees);
	}

	/**
	* Sets the employees associated with the department, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employeePKs the primary keys of the employees to be associated with the department
	*/
	public static void setEmployees(long pk, long[] employeePKs) {
		getPersistence().setEmployees(pk, employeePKs);
	}

	/**
	* Sets the employees associated with the department, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the department
	* @param employees the employees to be associated with the department
	*/
	public static void setEmployees(long pk,
		List<back.end.module.model.Employee> employees) {
		getPersistence().setEmployees(pk, employees);
	}

	public static DepartmentPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DepartmentPersistence, DepartmentPersistence> _serviceTracker =
		ServiceTrackerFactory.open(DepartmentPersistence.class);
}