/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package sms.functionality.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import sms.functionality.model.SMS;

import java.util.List;

/**
 * The persistence utility for the sms service. This utility wraps {@link sms.functionality.service.persistence.impl.SMSPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SMSPersistence
 * @see sms.functionality.service.persistence.impl.SMSPersistenceImpl
 * @generated
 */
@ProviderType
public class SMSUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(SMS sms) {
		getPersistence().clearCache(sms);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SMS> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SMS> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SMS> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<SMS> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static SMS update(SMS sms) {
		return getPersistence().update(sms);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static SMS update(SMS sms, ServiceContext serviceContext) {
		return getPersistence().update(sms, serviceContext);
	}

	/**
	* Returns all the smses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching smses
	*/
	public static List<SMS> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the smses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of matching smses
	*/
	public static List<SMS> findByUuid(java.lang.String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the smses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching smses
	*/
	public static List<SMS> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<SMS> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the smses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching smses
	*/
	public static List<SMS> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findByUuid_First(java.lang.String uuid,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<SMS> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findByUuid_Last(java.lang.String uuid,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<SMS> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the smses before and after the current sms in the ordered set where uuid = &#63;.
	*
	* @param smsId the primary key of the current sms
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public static SMS[] findByUuid_PrevAndNext(long smsId,
		java.lang.String uuid, OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence()
				   .findByUuid_PrevAndNext(smsId, uuid, orderByComparator);
	}

	/**
	* Removes all the smses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of smses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching smses
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the sms where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSMSException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findByUUID_G(java.lang.String uuid, long groupId)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the sms where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the sms where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the sms where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the sms that was removed
	*/
	public static SMS removeByUUID_G(java.lang.String uuid, long groupId)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of smses where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching smses
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the smses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching smses
	*/
	public static List<SMS> findByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the smses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of matching smses
	*/
	public static List<SMS> findByUuid_C(java.lang.String uuid, long companyId,
		int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the smses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching smses
	*/
	public static List<SMS> findByUuid_C(java.lang.String uuid, long companyId,
		int start, int end, OrderByComparator<SMS> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the smses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching smses
	*/
	public static List<SMS> findByUuid_C(java.lang.String uuid, long companyId,
		int start, int end, OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findByUuid_C_First(java.lang.String uuid, long companyId,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<SMS> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findByUuid_C_Last(java.lang.String uuid, long companyId,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		OrderByComparator<SMS> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the smses before and after the current sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param smsId the primary key of the current sms
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public static SMS[] findByUuid_C_PrevAndNext(long smsId,
		java.lang.String uuid, long companyId,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(smsId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the smses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of smses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching smses
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the smses where smsText = &#63;.
	*
	* @param smsText the sms text
	* @return the matching smses
	*/
	public static List<SMS> findBysmsText(java.lang.String smsText) {
		return getPersistence().findBysmsText(smsText);
	}

	/**
	* Returns a range of all the smses where smsText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param smsText the sms text
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of matching smses
	*/
	public static List<SMS> findBysmsText(java.lang.String smsText, int start,
		int end) {
		return getPersistence().findBysmsText(smsText, start, end);
	}

	/**
	* Returns an ordered range of all the smses where smsText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param smsText the sms text
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching smses
	*/
	public static List<SMS> findBysmsText(java.lang.String smsText, int start,
		int end, OrderByComparator<SMS> orderByComparator) {
		return getPersistence()
				   .findBysmsText(smsText, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the smses where smsText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param smsText the sms text
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching smses
	*/
	public static List<SMS> findBysmsText(java.lang.String smsText, int start,
		int end, OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBysmsText(smsText, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findBysmsText_First(java.lang.String smsText,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().findBysmsText_First(smsText, orderByComparator);
	}

	/**
	* Returns the first sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchBysmsText_First(java.lang.String smsText,
		OrderByComparator<SMS> orderByComparator) {
		return getPersistence().fetchBysmsText_First(smsText, orderByComparator);
	}

	/**
	* Returns the last sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public static SMS findBysmsText_Last(java.lang.String smsText,
		OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().findBysmsText_Last(smsText, orderByComparator);
	}

	/**
	* Returns the last sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public static SMS fetchBysmsText_Last(java.lang.String smsText,
		OrderByComparator<SMS> orderByComparator) {
		return getPersistence().fetchBysmsText_Last(smsText, orderByComparator);
	}

	/**
	* Returns the smses before and after the current sms in the ordered set where smsText = &#63;.
	*
	* @param smsId the primary key of the current sms
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public static SMS[] findBysmsText_PrevAndNext(long smsId,
		java.lang.String smsText, OrderByComparator<SMS> orderByComparator)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence()
				   .findBysmsText_PrevAndNext(smsId, smsText, orderByComparator);
	}

	/**
	* Removes all the smses where smsText = &#63; from the database.
	*
	* @param smsText the sms text
	*/
	public static void removeBysmsText(java.lang.String smsText) {
		getPersistence().removeBysmsText(smsText);
	}

	/**
	* Returns the number of smses where smsText = &#63;.
	*
	* @param smsText the sms text
	* @return the number of matching smses
	*/
	public static int countBysmsText(java.lang.String smsText) {
		return getPersistence().countBysmsText(smsText);
	}

	/**
	* Caches the sms in the entity cache if it is enabled.
	*
	* @param sms the sms
	*/
	public static void cacheResult(SMS sms) {
		getPersistence().cacheResult(sms);
	}

	/**
	* Caches the smses in the entity cache if it is enabled.
	*
	* @param smses the smses
	*/
	public static void cacheResult(List<SMS> smses) {
		getPersistence().cacheResult(smses);
	}

	/**
	* Creates a new sms with the primary key. Does not add the sms to the database.
	*
	* @param smsId the primary key for the new sms
	* @return the new sms
	*/
	public static SMS create(long smsId) {
		return getPersistence().create(smsId);
	}

	/**
	* Removes the sms with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param smsId the primary key of the sms
	* @return the sms that was removed
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public static SMS remove(long smsId)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().remove(smsId);
	}

	public static SMS updateImpl(SMS sms) {
		return getPersistence().updateImpl(sms);
	}

	/**
	* Returns the sms with the primary key or throws a {@link NoSuchSMSException} if it could not be found.
	*
	* @param smsId the primary key of the sms
	* @return the sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public static SMS findByPrimaryKey(long smsId)
		throws sms.functionality.exception.NoSuchSMSException {
		return getPersistence().findByPrimaryKey(smsId);
	}

	/**
	* Returns the sms with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param smsId the primary key of the sms
	* @return the sms, or <code>null</code> if a sms with the primary key could not be found
	*/
	public static SMS fetchByPrimaryKey(long smsId) {
		return getPersistence().fetchByPrimaryKey(smsId);
	}

	public static java.util.Map<java.io.Serializable, SMS> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the smses.
	*
	* @return the smses
	*/
	public static List<SMS> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of smses
	*/
	public static List<SMS> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of smses
	*/
	public static List<SMS> findAll(int start, int end,
		OrderByComparator<SMS> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of smses
	*/
	public static List<SMS> findAll(int start, int end,
		OrderByComparator<SMS> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the smses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of smses.
	*
	* @return the number of smses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static SMSPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SMSPersistence, SMSPersistence> _serviceTracker =
		ServiceTrackerFactory.open(SMSPersistence.class);
}