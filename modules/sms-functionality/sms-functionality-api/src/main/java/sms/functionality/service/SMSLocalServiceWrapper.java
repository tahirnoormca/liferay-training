/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package sms.functionality.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SMSLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SMSLocalService
 * @generated
 */
@ProviderType
public class SMSLocalServiceWrapper implements SMSLocalService,
	ServiceWrapper<SMSLocalService> {
	public SMSLocalServiceWrapper(SMSLocalService smsLocalService) {
		_smsLocalService = smsLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _smsLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _smsLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _smsLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _smsLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _smsLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _smsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of smses.
	*
	* @return the number of smses
	*/
	@Override
	public int getSMSsCount() {
		return _smsLocalService.getSMSsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _smsLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _smsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link sms.functionality.model.impl.SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _smsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link sms.functionality.model.impl.SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _smsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link sms.functionality.model.impl.SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of smses
	*/
	@Override
	public java.util.List<sms.functionality.model.SMS> getSMSs(int start,
		int end) {
		return _smsLocalService.getSMSs(start, end);
	}

	/**
	* Returns all the smses matching the UUID and company.
	*
	* @param uuid the UUID of the smses
	* @param companyId the primary key of the company
	* @return the matching smses, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<sms.functionality.model.SMS> getSMSsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _smsLocalService.getSMSsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of smses matching the UUID and company.
	*
	* @param uuid the UUID of the smses
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching smses, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<sms.functionality.model.SMS> getSMSsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<sms.functionality.model.SMS> orderByComparator) {
		return _smsLocalService.getSMSsByUuidAndCompanyId(uuid, companyId,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _smsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _smsLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public sms.functionality.model.SMS addSMS(java.lang.String smsText) {
		return _smsLocalService.addSMS(smsText);
	}

	/**
	* Adds the sms to the database. Also notifies the appropriate model listeners.
	*
	* @param sms the sms
	* @return the sms that was added
	*/
	@Override
	public sms.functionality.model.SMS addSMS(sms.functionality.model.SMS sms) {
		return _smsLocalService.addSMS(sms);
	}

	/**
	* Creates a new sms with the primary key. Does not add the sms to the database.
	*
	* @param smsId the primary key for the new sms
	* @return the new sms
	*/
	@Override
	public sms.functionality.model.SMS createSMS(long smsId) {
		return _smsLocalService.createSMS(smsId);
	}

	/**
	* Deletes the sms with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param smsId the primary key of the sms
	* @return the sms that was removed
	* @throws PortalException if a sms with the primary key could not be found
	*/
	@Override
	public sms.functionality.model.SMS deleteSMS(long smsId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _smsLocalService.deleteSMS(smsId);
	}

	/**
	* Deletes the sms from the database. Also notifies the appropriate model listeners.
	*
	* @param sms the sms
	* @return the sms that was removed
	*/
	@Override
	public sms.functionality.model.SMS deleteSMS(
		sms.functionality.model.SMS sms) {
		return _smsLocalService.deleteSMS(sms);
	}

	@Override
	public sms.functionality.model.SMS fetchSMS(long smsId) {
		return _smsLocalService.fetchSMS(smsId);
	}

	/**
	* Returns the sms matching the UUID and group.
	*
	* @param uuid the sms's UUID
	* @param groupId the primary key of the group
	* @return the matching sms, or <code>null</code> if a matching sms could not be found
	*/
	@Override
	public sms.functionality.model.SMS fetchSMSByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _smsLocalService.fetchSMSByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the sms with the primary key.
	*
	* @param smsId the primary key of the sms
	* @return the sms
	* @throws PortalException if a sms with the primary key could not be found
	*/
	@Override
	public sms.functionality.model.SMS getSMS(long smsId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _smsLocalService.getSMS(smsId);
	}

	/**
	* Returns the sms matching the UUID and group.
	*
	* @param uuid the sms's UUID
	* @param groupId the primary key of the group
	* @return the matching sms
	* @throws PortalException if a matching sms could not be found
	*/
	@Override
	public sms.functionality.model.SMS getSMSByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _smsLocalService.getSMSByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the sms in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param sms the sms
	* @return the sms that was updated
	*/
	@Override
	public sms.functionality.model.SMS updateSMS(
		sms.functionality.model.SMS sms) {
		return _smsLocalService.updateSMS(sms);
	}

	@Override
	public SMSLocalService getWrappedService() {
		return _smsLocalService;
	}

	@Override
	public void setWrappedService(SMSLocalService smsLocalService) {
		_smsLocalService = smsLocalService;
	}

	private SMSLocalService _smsLocalService;
}