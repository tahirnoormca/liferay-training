/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package sms.functionality.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import sms.functionality.exception.NoSuchSMSException;

import sms.functionality.model.SMS;

/**
 * The persistence interface for the sms service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see sms.functionality.service.persistence.impl.SMSPersistenceImpl
 * @see SMSUtil
 * @generated
 */
@ProviderType
public interface SMSPersistence extends BasePersistence<SMS> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SMSUtil} to access the sms persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the smses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching smses
	*/
	public java.util.List<SMS> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the smses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of matching smses
	*/
	public java.util.List<SMS> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the smses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching smses
	*/
	public java.util.List<SMS> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns an ordered range of all the smses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching smses
	*/
	public java.util.List<SMS> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Returns the first sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns the last sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Returns the last sms in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns the smses before and after the current sms in the ordered set where uuid = &#63;.
	*
	* @param smsId the primary key of the current sms
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public SMS[] findByUuid_PrevAndNext(long smsId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Removes all the smses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of smses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching smses
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the sms where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSMSException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSMSException;

	/**
	* Returns the sms where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the sms where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the sms where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the sms that was removed
	*/
	public SMS removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSMSException;

	/**
	* Returns the number of smses where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching smses
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the smses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching smses
	*/
	public java.util.List<SMS> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the smses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of matching smses
	*/
	public java.util.List<SMS> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the smses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching smses
	*/
	public java.util.List<SMS> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns an ordered range of all the smses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching smses
	*/
	public java.util.List<SMS> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Returns the first sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns the last sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Returns the last sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns the smses before and after the current sms in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param smsId the primary key of the current sms
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public SMS[] findByUuid_C_PrevAndNext(long smsId, java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Removes all the smses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of smses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching smses
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the smses where smsText = &#63;.
	*
	* @param smsText the sms text
	* @return the matching smses
	*/
	public java.util.List<SMS> findBysmsText(java.lang.String smsText);

	/**
	* Returns a range of all the smses where smsText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param smsText the sms text
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of matching smses
	*/
	public java.util.List<SMS> findBysmsText(java.lang.String smsText,
		int start, int end);

	/**
	* Returns an ordered range of all the smses where smsText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param smsText the sms text
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching smses
	*/
	public java.util.List<SMS> findBysmsText(java.lang.String smsText,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns an ordered range of all the smses where smsText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param smsText the sms text
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching smses
	*/
	public java.util.List<SMS> findBysmsText(java.lang.String smsText,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findBysmsText_First(java.lang.String smsText,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Returns the first sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchBysmsText_First(java.lang.String smsText,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns the last sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms
	* @throws NoSuchSMSException if a matching sms could not be found
	*/
	public SMS findBysmsText_Last(java.lang.String smsText,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Returns the last sms in the ordered set where smsText = &#63;.
	*
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sms, or <code>null</code> if a matching sms could not be found
	*/
	public SMS fetchBysmsText_Last(java.lang.String smsText,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns the smses before and after the current sms in the ordered set where smsText = &#63;.
	*
	* @param smsId the primary key of the current sms
	* @param smsText the sms text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public SMS[] findBysmsText_PrevAndNext(long smsId,
		java.lang.String smsText,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator)
		throws NoSuchSMSException;

	/**
	* Removes all the smses where smsText = &#63; from the database.
	*
	* @param smsText the sms text
	*/
	public void removeBysmsText(java.lang.String smsText);

	/**
	* Returns the number of smses where smsText = &#63;.
	*
	* @param smsText the sms text
	* @return the number of matching smses
	*/
	public int countBysmsText(java.lang.String smsText);

	/**
	* Caches the sms in the entity cache if it is enabled.
	*
	* @param sms the sms
	*/
	public void cacheResult(SMS sms);

	/**
	* Caches the smses in the entity cache if it is enabled.
	*
	* @param smses the smses
	*/
	public void cacheResult(java.util.List<SMS> smses);

	/**
	* Creates a new sms with the primary key. Does not add the sms to the database.
	*
	* @param smsId the primary key for the new sms
	* @return the new sms
	*/
	public SMS create(long smsId);

	/**
	* Removes the sms with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param smsId the primary key of the sms
	* @return the sms that was removed
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public SMS remove(long smsId) throws NoSuchSMSException;

	public SMS updateImpl(SMS sms);

	/**
	* Returns the sms with the primary key or throws a {@link NoSuchSMSException} if it could not be found.
	*
	* @param smsId the primary key of the sms
	* @return the sms
	* @throws NoSuchSMSException if a sms with the primary key could not be found
	*/
	public SMS findByPrimaryKey(long smsId) throws NoSuchSMSException;

	/**
	* Returns the sms with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param smsId the primary key of the sms
	* @return the sms, or <code>null</code> if a sms with the primary key could not be found
	*/
	public SMS fetchByPrimaryKey(long smsId);

	@Override
	public java.util.Map<java.io.Serializable, SMS> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the smses.
	*
	* @return the smses
	*/
	public java.util.List<SMS> findAll();

	/**
	* Returns a range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @return the range of smses
	*/
	public java.util.List<SMS> findAll(int start, int end);

	/**
	* Returns an ordered range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of smses
	*/
	public java.util.List<SMS> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator);

	/**
	* Returns an ordered range of all the smses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SMSModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of smses
	* @param end the upper bound of the range of smses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of smses
	*/
	public java.util.List<SMS> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SMS> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the smses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of smses.
	*
	* @return the number of smses
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}