/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package sms.functionality.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link SMS}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SMS
 * @generated
 */
@ProviderType
public class SMSWrapper implements SMS, ModelWrapper<SMS> {
	public SMSWrapper(SMS sms) {
		_sms = sms;
	}

	@Override
	public Class<?> getModelClass() {
		return SMS.class;
	}

	@Override
	public String getModelClassName() {
		return SMS.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("smsId", getSmsId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("smsText", getSmsText());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long smsId = (Long)attributes.get("smsId");

		if (smsId != null) {
			setSmsId(smsId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String smsText = (String)attributes.get("smsText");

		if (smsText != null) {
			setSmsText(smsText);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _sms.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _sms.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _sms.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _sms.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<sms.functionality.model.SMS> toCacheModel() {
		return _sms.toCacheModel();
	}

	@Override
	public int compareTo(sms.functionality.model.SMS sms) {
		return _sms.compareTo(sms);
	}

	@Override
	public int hashCode() {
		return _sms.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _sms.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SMSWrapper((SMS)_sms.clone());
	}

	/**
	* Returns the sms text of this sms.
	*
	* @return the sms text of this sms
	*/
	@Override
	public java.lang.String getSmsText() {
		return _sms.getSmsText();
	}

	/**
	* Returns the user name of this sms.
	*
	* @return the user name of this sms
	*/
	@Override
	public java.lang.String getUserName() {
		return _sms.getUserName();
	}

	/**
	* Returns the user uuid of this sms.
	*
	* @return the user uuid of this sms
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _sms.getUserUuid();
	}

	/**
	* Returns the uuid of this sms.
	*
	* @return the uuid of this sms
	*/
	@Override
	public java.lang.String getUuid() {
		return _sms.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _sms.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _sms.toXmlString();
	}

	/**
	* Returns the create date of this sms.
	*
	* @return the create date of this sms
	*/
	@Override
	public Date getCreateDate() {
		return _sms.getCreateDate();
	}

	/**
	* Returns the modified date of this sms.
	*
	* @return the modified date of this sms
	*/
	@Override
	public Date getModifiedDate() {
		return _sms.getModifiedDate();
	}

	/**
	* Returns the company ID of this sms.
	*
	* @return the company ID of this sms
	*/
	@Override
	public long getCompanyId() {
		return _sms.getCompanyId();
	}

	/**
	* Returns the group ID of this sms.
	*
	* @return the group ID of this sms
	*/
	@Override
	public long getGroupId() {
		return _sms.getGroupId();
	}

	/**
	* Returns the primary key of this sms.
	*
	* @return the primary key of this sms
	*/
	@Override
	public long getPrimaryKey() {
		return _sms.getPrimaryKey();
	}

	/**
	* Returns the sms ID of this sms.
	*
	* @return the sms ID of this sms
	*/
	@Override
	public long getSmsId() {
		return _sms.getSmsId();
	}

	/**
	* Returns the user ID of this sms.
	*
	* @return the user ID of this sms
	*/
	@Override
	public long getUserId() {
		return _sms.getUserId();
	}

	@Override
	public sms.functionality.model.SMS toEscapedModel() {
		return new SMSWrapper(_sms.toEscapedModel());
	}

	@Override
	public sms.functionality.model.SMS toUnescapedModel() {
		return new SMSWrapper(_sms.toUnescapedModel());
	}

	@Override
	public void persist() {
		_sms.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_sms.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this sms.
	*
	* @param companyId the company ID of this sms
	*/
	@Override
	public void setCompanyId(long companyId) {
		_sms.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this sms.
	*
	* @param createDate the create date of this sms
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_sms.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_sms.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_sms.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_sms.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this sms.
	*
	* @param groupId the group ID of this sms
	*/
	@Override
	public void setGroupId(long groupId) {
		_sms.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this sms.
	*
	* @param modifiedDate the modified date of this sms
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_sms.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_sms.setNew(n);
	}

	/**
	* Sets the primary key of this sms.
	*
	* @param primaryKey the primary key of this sms
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_sms.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_sms.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the sms ID of this sms.
	*
	* @param smsId the sms ID of this sms
	*/
	@Override
	public void setSmsId(long smsId) {
		_sms.setSmsId(smsId);
	}

	/**
	* Sets the sms text of this sms.
	*
	* @param smsText the sms text of this sms
	*/
	@Override
	public void setSmsText(java.lang.String smsText) {
		_sms.setSmsText(smsText);
	}

	/**
	* Sets the user ID of this sms.
	*
	* @param userId the user ID of this sms
	*/
	@Override
	public void setUserId(long userId) {
		_sms.setUserId(userId);
	}

	/**
	* Sets the user name of this sms.
	*
	* @param userName the user name of this sms
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_sms.setUserName(userName);
	}

	/**
	* Sets the user uuid of this sms.
	*
	* @param userUuid the user uuid of this sms
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_sms.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this sms.
	*
	* @param uuid the uuid of this sms
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_sms.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SMSWrapper)) {
			return false;
		}

		SMSWrapper smsWrapper = (SMSWrapper)obj;

		if (Objects.equals(_sms, smsWrapper._sms)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _sms.getStagedModelType();
	}

	@Override
	public SMS getWrappedModel() {
		return _sms;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _sms.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _sms.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_sms.resetOriginalValues();
	}

	private final SMS _sms;
}