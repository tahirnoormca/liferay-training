create index IX_D4273F5A on SMSService_SMS (smsText[$COLUMN_LENGTH:75$]);
create index IX_D494D6F0 on SMSService_SMS (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_86465972 on SMSService_SMS (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_E63E9FBD on SMS_SMS (smsText[$COLUMN_LENGTH:75$]);
create index IX_2F3035ED on SMS_SMS (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_D93753AF on SMS_SMS (uuid_[$COLUMN_LENGTH:75$], groupId);