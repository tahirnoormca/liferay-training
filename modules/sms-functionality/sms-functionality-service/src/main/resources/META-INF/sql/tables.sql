create table SMSService_SMS (
	uuid_ VARCHAR(75) null,
	smsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	smsText VARCHAR(75) null
);

create table SMS_SMS (
	uuid_ VARCHAR(75) null,
	smsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	smsText VARCHAR(75) null
);