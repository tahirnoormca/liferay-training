/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package sms.functionality.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import sms.functionality.model.SMS;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing SMS in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see SMS
 * @generated
 */
@ProviderType
public class SMSCacheModel implements CacheModel<SMS>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SMSCacheModel)) {
			return false;
		}

		SMSCacheModel smsCacheModel = (SMSCacheModel)obj;

		if (smsId == smsCacheModel.smsId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, smsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", smsId=");
		sb.append(smsId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", smsText=");
		sb.append(smsText);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SMS toEntityModel() {
		SMSImpl smsImpl = new SMSImpl();

		if (uuid == null) {
			smsImpl.setUuid(StringPool.BLANK);
		}
		else {
			smsImpl.setUuid(uuid);
		}

		smsImpl.setSmsId(smsId);
		smsImpl.setGroupId(groupId);
		smsImpl.setCompanyId(companyId);
		smsImpl.setUserId(userId);

		if (userName == null) {
			smsImpl.setUserName(StringPool.BLANK);
		}
		else {
			smsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			smsImpl.setCreateDate(null);
		}
		else {
			smsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			smsImpl.setModifiedDate(null);
		}
		else {
			smsImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (smsText == null) {
			smsImpl.setSmsText(StringPool.BLANK);
		}
		else {
			smsImpl.setSmsText(smsText);
		}

		smsImpl.resetOriginalValues();

		return smsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		smsId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		smsText = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(smsId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (smsText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(smsText);
		}
	}

	public String uuid;
	public long smsId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String smsText;
}