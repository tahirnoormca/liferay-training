/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package sms.functionality.service.impl;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;

import aQute.bnd.annotation.ProviderType;
import sms.functionality.model.SMS;
import sms.functionality.service.base.SMSLocalServiceBaseImpl;

/**
 * The implementation of the sms local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link sms.functionality.service.SMSLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SMSLocalServiceBaseImpl
 * @see sms.functionality.service.SMSLocalServiceUtil
 */
@ProviderType
public class SMSLocalServiceImpl extends SMSLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link sms.functionality.service.SMSLocalServiceUtil} to access the sms local service.
	 */
	public SMS addSMS(String smsText){
		SMS sms=smsPersistence.create(CounterLocalServiceUtil.increment());
		sms.setSmsText(smsText);
		return smsPersistence.update(sms);
		
	}
}