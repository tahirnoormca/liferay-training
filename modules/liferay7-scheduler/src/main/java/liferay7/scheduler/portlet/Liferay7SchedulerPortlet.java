package liferay7.scheduler.portlet;

import com.liferay.portal.kernel.messaging.BaseSchedulerEntryMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelper;
import com.liferay.portal.kernel.scheduler.TimeUnit;
import com.liferay.portal.kernel.scheduler.TriggerFactoryUtil;
import java.util.Date;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tana0616
 */
@Component(immediate = true, service = MessageListener.class)
public class Liferay7SchedulerPortlet extends BaseSchedulerEntryMessageListener  {

	@Override
	protected void doReceive(Message message) throws Exception {
		System.out.println("MySchedular.doReceive()" + new Date());		
	}
	@Activate
	@Modified
	protected void activate() {
		System.out.println("activate()");
		schedulerEntryImpl.setTrigger(TriggerFactoryUtil.createTrigger(getEventListenerClass(), getEventListenerClass(),
				5, TimeUnit.SECOND));
		_schedulerEngineHelper.register(this, schedulerEntryImpl, DestinationNames.SCHEDULER_DISPATCH);
		
	}
	@Deactivate
	protected void deactivate() {
		System.out.println("deactivate()");
		_schedulerEngineHelper.unregister(this);
	}		
	@Reference(unbind = "-")
	private SchedulerEngineHelper _schedulerEngineHelper;

}