package service.hook.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.UserLocalServiceWrapper;

import java.util.Map;



import org.osgi.service.component.annotations.Component;

/**
 * @author tana0616
 */
@Component(immediate = true, property = {}, service = ServiceWrapper.class)
public class ServiceHookPortlet  extends UserLocalServiceWrapper{

	public ServiceHookPortlet() {
		super(null);		
	}
	@Override
	public int authenticateByEmailAddress(long companyId, String emailAddress, String password,
			Map<String, String[]> headerMap, Map<String, String[]> parameterMap, Map<String, Object> resultsMap)
			throws PortalException {
		System.out.println("Hellooooooooooo from service");
		return super.authenticateByEmailAddress(companyId, emailAddress, password, headerMap, parameterMap, resultsMap);
	}
}