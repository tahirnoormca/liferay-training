package com.tahir.adminportlet.portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;

@Component(
	    immediate = true,
	    property = {
	        "panel.app.order:Integer=100",
	        "panel.category.key=" + PanelCategoryKeys.CONTROL_PANEL_USERS
	    },
	   service = PanelApp.class
)
public class AdminPanelApplication extends BasePanelApp {

	 @Override
	    public String getPortletId() {
		return "ProductAdminn";
	    }

	    @Override
	    @Reference(target = "(javax.portlet.name=ProductAdminn)",unbind = "-")
	    public void setPortlet(Portlet portlet) {
	        super.setPortlet(portlet);
	    }	

}
