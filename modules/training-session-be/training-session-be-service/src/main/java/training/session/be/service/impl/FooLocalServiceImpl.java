/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package training.session.be.service.impl;



import java.util.List;



import aQute.bnd.annotation.ProviderType;
import training.session.be.model.Foo;
import training.session.be.service.base.FooLocalServiceBaseImpl;
import training.session.be.service.persistence.FooUtil;

/**
 * The implementation of the foo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link training.session.be.service.FooLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FooLocalServiceBaseImpl
 * @see training.session.be.service.FooLocalServiceUtil
 */
@ProviderType
public class FooLocalServiceImpl extends FooLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link training.session.be.service.FooLocalServiceUtil} to access the foo local service.
	 */
	
	
	public Foo addInToFoo(String name) {
		long fooId = counterLocalService.increment();
		Foo foo = fooPersistence.create(fooId);
		foo.setName(name);			
		return  fooPersistence.update(foo);
	}
	
	public List<Foo> findFooByName(String name){
		return fooPersistence.findByname(name);
	}
}