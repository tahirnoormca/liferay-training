create index IX_B02EE5C6 on training_Foo (name[$COLUMN_LENGTH:75$]);
create index IX_D973457B on training_Foo (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_5C74EEBD on training_Foo (uuid_[$COLUMN_LENGTH:75$], groupId);