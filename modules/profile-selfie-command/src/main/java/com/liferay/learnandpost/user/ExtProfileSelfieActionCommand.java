/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liferay.learnandpost.user;

import java.io.File;
import java.nio.file.Files;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;

@Component(
	immediate = true,
	property = {
			"javax.portlet.name=com_liferay_my_account_web_portlet_MyAccountPortlet",
			"mvc.command.name=/users_admin/edit_user",
	        "service.ranking:Integer=100"
    },
    service = MVCActionCommand.class
)
public class ExtProfileSelfieActionCommand extends BaseMVCActionCommand {
	protected MVCActionCommand mvcActionCommand;

	@Reference
	protected UserLocalService userLocalService;
	
    @Reference(target = "(&(mvc.command.name=/users_admin/edit_user)" +
            "(javax.portlet.name=com_liferay_my_account_web_portlet_MyAccountPortlet)"+
            "(component.name=com.liferay.my.account.web.internal.portlet.action.EditUserMVCActionCommand))")
    public void setMvcActionCommand(MVCActionCommand mvcActionCommand) {
        this.mvcActionCommand = mvcActionCommand;
    }

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		_log.info("User Edit Controller");
		
		String fileInputName = "fileNameCapture";
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		if (uploadRequest.getSize(fileInputName) != 0) {
			// Get the uploaded file as a file.
			File uploadedFile = uploadRequest.getFile(fileInputName);
			System.out.println("uploadedFile >> " + uploadedFile);
			
			byte fileContent[] = Files.readAllBytes(uploadedFile.toPath());
			System.out.println("fileContent >> " + fileContent);

			User user = PortalUtil.getUser(actionRequest);
			System.out.println("user name > " + user.getFullName());
			
			try {
				userLocalService.updatePortrait(user.getUserId(), fileContent);	
			} catch(Exception e ){
				_log.error(e.getMessage());
				_log.error(e.getCause());
			}
			
			  /*PortalUtil.updateImageId( user, true, fileContent, "portraitId",
			  PrefsPropsUtil.getLong(PropsKeys.USERS_IMAGE_MAX_SIZE),
			  PropsValues.USERS_IMAGE_MAX_HEIGHT,
			  PropsValues.USERS_IMAGE_MAX_WIDTH);*/
			 
			  System.out.println("Image update code");
		} else {
			_log.info("Received file is 0 bytes!");
		}
		
		mvcActionCommand.processAction(actionRequest, actionResponse);
		System.out.println("Process Action end");
	}

	private static final Log _log = LogFactoryUtil.getLog(
		ExtProfileSelfieActionCommand.class);
}