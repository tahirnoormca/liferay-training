package customer.service.wrapper;

import back.end.module.model.Employee;
import back.end.module.service.EmployeeLocalServiceWrapper;


import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.service.component.annotations.Component;

/**
 * @author tana0616
 */
@Component(
	immediate = true,
	property = {
	},
	service = ServiceWrapper.class
)
public class CustomerServiceWrapper extends EmployeeLocalServiceWrapper {

	public CustomerServiceWrapper() {
		super(null);
	}
	@Override
	public Employee addEmployee(String employeeName, String address) {
		System.out.println("this is Custom Service wrapper");
		return super.addEmployee(employeeName, address);
	}
	

}