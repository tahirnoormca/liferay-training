package login.action.hook.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tana0616
 */
@Component(
		property = {
                "javax.portlet.name=com_liferay_login_web_portlet_LoginPortlet",
				"mvc.command.name=/login/login",
                "service.ranking:Integer=1000"
		},
		service = MVCActionCommand.class)
public class LoginActionHookPortlet extends BaseMVCActionCommand {
	
	 protected MVCActionCommand mvcActionCommand;

	    @Reference(target = "(&(mvc.command.name=/login/login)" +
	            "(javax.portlet.name=com_liferay_login_web_portlet_LoginPortlet)"+
	            "(component.name=com.liferay.login.web.internal.portlet.action.LoginMVCActionCommand))")
	    public void setMvcActionCommand(MVCActionCommand mvcActionCommand) {
	        this.mvcActionCommand = mvcActionCommand;
	    }

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		 System.out.println(">>>>>>>>>>>>>>>>>>>OVERRIDING MVCACTIONCOMMAND<<<<<<<<<<<<<<<<<<<<<< ");
	        mvcActionCommand.processAction(actionRequest,actionResponse);
		
	}
}