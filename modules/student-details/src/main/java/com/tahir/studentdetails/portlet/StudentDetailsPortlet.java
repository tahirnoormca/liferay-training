package com.tahir.studentdetails.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import back.end.module.service.EmployeeLocalService;

/**
 * @author tana0616
 */
@Component(
		immediate = true,
		property = {
	        "javax.portlet.name=liferayTrainingSession",
	        "mvc.command.name=/test/showdetails"
	    },
	    service = MVCRenderCommand.class
	)
public class StudentDetailsPortlet implements MVCRenderCommand {

	@Reference(bind="-")
	private EmployeeLocalService employeeLocalService;
	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {	
		System.out.println("==================================");
		renderRequest.setAttribute("employeeList",employeeLocalService.getEmployees(0, employeeLocalService.getEmployeesCount()));
		return "/jsps/student-details.jsp";
	}
}