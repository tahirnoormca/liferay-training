package com.liferay.api;

import java.util.Collections;
import java.util.Enumeration;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.UTF8Control;

@Component(
		property = 	{
				"language.id=en_US" 
				},
		service = ResourceBundle.class)
public class MyLanguageBundle extends ResourceBundle {

	@Override
	protected Object handleGetObject(String key) {
		System.out.println("Key :- "+key+" _resourceBundle.getObject(key) :- "+_resourceBundle.getObject(key));
		return _resourceBundle.getObject(key);
	}

	@Override
	public Enumeration<String> getKeys() {			
		for (String key : Collections.list(_resourceBundle.getKeys()))
		    System.out.println("keys ::: +"+key);
		return _resourceBundle.getKeys();
	}

	private final ResourceBundle _resourceBundle = ResourceBundle.getBundle("content.Language", UTF8Control.INSTANCE);

}