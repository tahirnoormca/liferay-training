<%@ include file="init.jsp" %>
<portlet:actionURL name="checkuseractioncommand" var="checkUserURL" />
 <portlet:resourceURL var="testAjaxResourceUrl" id="/test/osgi/resourceUrl"/>
<p>
	<b><liferay-ui:message key="portlet-jspview.caption"/></b>
	<liferay-ui:success key="greetingMessage" message="Successfully" />
	
	
</p>
<div class="container">  
  <form class="form-horizontal" action="<%=checkUserURL%>" method="post" >
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Date of Birth:</label>
      <div class="col-sm-10">          
        <input type="date" class="form-control" id="dob" placeholder="Enter DoB" name="dob">
      </div>
    </div>   
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form> 
<button onclick="ajaxCall()">resourceURL in Ajax</button>
</div>
<script type="text/javascript">

    function ajaxCall() {
        AUI().use('aui-io-request', function (A) {
            A.io.request('${testAjaxResourceUrl}', {
                method: 'post',
                data: {
                    <portlet:namespace/>sampleParam: 'Hello from jsp',
                },
                on: {
                    success: function () {
                        alert(this.get('responseData'));
                    }

                }
            });
        });
    }
</script>