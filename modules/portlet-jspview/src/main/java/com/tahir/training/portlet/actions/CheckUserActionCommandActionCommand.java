/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tahir.training.portlet.actions;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
        "javax.portlet.name=liferayTrainingAction",
        "mvc.command.name=checkuseractioncommand"
    },
    service = MVCActionCommand.class
)
public class CheckUserActionCommandActionCommand extends BaseMVCActionCommand {
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		_handleActionCommand( actionRequest);		
	}	

	private void _handleActionCommand(ActionRequest actionRequest) {
		_log.info("Action Command");
		String email = ParamUtil.get(actionRequest, "email", StringPool.BLANK);
		String dob = ParamUtil.get(actionRequest, "dob", StringPool.BLANK);

		if (_log.isInfoEnabled()) {
			_log.info("Hello " + email);
		}
		String greetingMessage = "Hello " + email + "! Welcome to OSGi, born on : "+dob;
		actionRequest.setAttribute("GREETER_MESSAGE", greetingMessage);
		SessionMessages.add(actionRequest, "greetingMessage", greetingMessage);
	}

	private static final Log _log = LogFactoryUtil.getLog(
		CheckUserActionCommandActionCommand.class);

	
}