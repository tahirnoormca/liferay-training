/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tahir.training.portlet.actions;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import java.io.IOException;
import java.io.PrintWriter;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"javax.portlet.name=liferayTrainingAction",
        "mvc.command.name=/test/osgi/resourceUrl"
    },
    service = MVCResourceCommand.class
)
public class CheckUserResourceCommand extends BaseMVCResourceCommand  {
	
	private static final Log _log = LogFactoryUtil.getLog(CheckUserResourceCommand.class);

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {		
		_log.info("Ajax");
		resourceResponse.setContentType("text/html");
		PrintWriter out;
		try {
		out = resourceResponse.getWriter();		
		out.println("AUI Ajax call is performed");
		out.flush();
		} catch (IOException e) {			
			e.printStackTrace();
		} 
		
	}
}