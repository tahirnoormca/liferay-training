package com.tahir.training.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author tana0616
 */
@Component(
	immediate = true,
	property = {
		"javax.portlet.name=liferayTrainingAction",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=portlet-jspview Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		
	},
	service = Portlet.class
)
public class JspViewPortlet extends MVCPortlet {
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		/*getPortletContext().getRequestDispatcher("").include(renderRequest, renderResponse);*/
		readPropertiesFile();
		super.doView(renderRequest, renderResponse);
	}
	private void readPropertiesFile() {
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "portlet.properties";
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
				System.out.println(">>>>>>>>>>>>>>"+prop.getProperty("tahir.article.id"));

			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}

	}
	
}